# mr-start #

Installing this repository will bootstrap dotfiles in your user accont.

### How do I get set up? ###

```
yaourt -S myrepos vcsh
vcsh clone https://bitbucket.org/k2s/mr-start.git mr
mr up
# maybe you will need to solve conflicts with existing files and repeat mr up command

# then it is advisable to install needed software with
# open new zsh shell and
,pa-sync-apps
```

### Where to submit ideas and issues ###

use https://bitbucket.org/k2s/dot_general/issues